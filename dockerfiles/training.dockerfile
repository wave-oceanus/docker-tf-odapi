# SETUP:
# docker build -t training -f ./dockerfiles/training.dockerfile ./dockerfiles
# CPU: docker run --rm -it -p 8888:8888 -v ${pwd}:/home/oceanus training
# GPU: docker run --rm -it -p 8888:8888 --gpus all -v ${pwd}:/home/oceanus training

FROM tensorflow/tensorflow:1.15.0-py3

# make a "user" directory
RUN mkdir -p /home/oceanus
WORKDIR /home

ENV TZ=Europe/Lisbon
RUN ln -snf /usr/share/zoneinfo/$TZ /etc/localtime && echo $TZ > /etc/timezone

RUN apt update
RUN apt install -qq protobuf-compiler python-pil python-lxml python-tk git wget -y

RUN pip install numpy==1.17.4
RUN pip install Cython contextlib2 pillow lxml matplotlib absl-py pandas notebook
RUN pip install pycocotools

# jupyter at startup script
COPY docker-entrypoint.sh /usr/local/bin/
RUN ln -s /usr/local/bin/docker-entrypoint.sh /
ENTRYPOINT ["sh", "/docker-entrypoint.sh"]

# return home
ENV JUPYTER_RUNTIME_DIR /tmp
ENV HOME /home/oceanus
USER 1000:1000
WORKDIR $HOME
